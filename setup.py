import setuptools
from distutils.core import setup
import numpy


setup( 
        name='unitoy', 
        version='0.1.5.dev1', 
        description='tools for electric markets', 
        author='Juan Pablo Luna', 
        author_email='jpluna@yandex.com', 
        license='MIT',
        packages=['unitoy',],
        install_requires=['pandas', 'numpy'],
        include_package_data=True,
        # package_data={'': ['data/*.csv']},
        package_data={'': ['data/**/*']},
        )
