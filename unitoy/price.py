import numpy as np
import pandas as pd
import gurobipy as grb
from unitoy import toys as ut

def linearRelaxation(toy, MIPGap=None):
    price = None
    optVal = None
    primalResult = None
    extraResult = None

    if toy.primalDict is None:
        print('Primal model not yet built...')
    else:
        linRelaxDict = ut.copyModelDict(toy.primalDict, relax=True)
    dual = []
    optVal, primalResult, extraResult = ut.solveModelDict(linRelaxDict, dual=dual, MIPGap=MIPGap)

    if dual:
        price = dual[0]['demand']
    else:
        price = None

    return price, optVal, primalResult, extraResult

def IP(toy):
    price = None
    optVal = None
    primalResult = None
    extraResult = None
    if toy.primalDict is None:
        print('Primal model not yet built...')
    else:
        rOptVAl, rPrimalResult , extraResult= toy.solveModel()
        if rOptVAl is not None:
            linRelaxDict = ut.copyModelDict(toy.primalDict, relax=True)
            u = linRelaxDict['var']['commit(u)']
            
            uCon= linRelaxDict['model'].addConstrs((u[i] == rPrimalResult.loc[i,'commit(u)'] for i in u), name='commit_fixed')
            linRelaxDict['extraCon']['commit_fixed'] = uCon
            dual = []
            optVal, primalResult, extraResult = ut.solveModelDict(linRelaxDict, dual=dual)
            if dual:
                price = dual[0]
            else:
                price = None
        else:
            print('primal model failed to be solved')

    return price, optVal, primalResult, extraResult

def minUplift(toy, price=None, maxIter=100,globalLog=None, xLog=None, nsoMethod='python',tol=1e-3, forcePD=None):
    import pynso.proxBundle as pb
    price = None
    optVal = None
    primalResult = None
    extraResult = None

    if toy.primalDict is None:
        print('Primal model not yet built...')
        price = None
        result = None
    else: 
        if price is None: 
            price, optVal, primalResult, extraResult = linearRelaxation(toy)
        else:
            optVal=0 # just to make next if work

        if optVal is not None:
            def bb(lmda, mode=0, context=None): 
                val, superGrad, status = toy.LR_BB(lmda)
                if mode == 0:
                    return -val, status
                elif mode == 1:
                    return -superGrad, status
                elif mode == 2:
                    return -val, -superGrad, status
            def fg(lmda):
                val, superGrad, status = toy.LR_BB(lmda) 
                return -val, -superGrad

            if forcePD is not None:
                solveProxMConxtext = {'forcePD': forcePD}
            else:
                solveProxMConxtext = None

            result = pb.minimize(price.values, fg=fg, maxIter=maxIter, globalLog=globalLog, xLog=xLog, method=nsoMethod, tol=tol, solveProxMConxtext = solveProxMConxtext)
            if result.status !=0:
                print('nonsmooth solver failed to find dual solution. status={}'.format(result.status))
                price = result.x_best
        else:
            print('relaxed model failed to be solved')
            price = None
            result = None

    return price, result


# def econ(toy, priceTarget=None, beta=0.1, dispatchPlan=None):
    # price = None
    # optVal = None
    # if toy.primalDict is None:
        # print('Primal model not yet built...')
    # else:
        # if dispatchPlan is None:
            # print('No dispatch plan provided. It will be computed from toy')
            # rOptVAl, rPrimalResult , extraResult= toy.solveModel()
            # if rOptVAl is not None:
                
                # uCon= linRelaxDict['model'].addConstrs((u[i] == rPrimalResult.loc[i,'commit(u)'] for i in u), name='commit_fixed')
                # linRelaxDict['extraCon']['commit_fixed'] = uCon
                # dual = []
                # optVal, primalResult, extraResult = ut.solveModelDict(linRelaxDict, dual=dual)
                # if dual:
                    # price = dual[0]
                # else:
                    # price = None
            # else:
                # print('primal model failed to be solved')

    # return price, optVal


#### Claudia ra_pricesH.py

"""
Little module to implement RA prices and utilities.
"""



def fixed_cost(data, result, u):
    """Compute fixed cost of a Unitoy solution.
    """
    commit = result.loc[u]["commit(u)"].values
    fixed_cost = data.loc[u, "F"]
    state_0 = data.loc[u, "state0"]
    on_cost = max(0.0, commit[0] - state_0)*fixed_cost
    on_cost += np.sum(np.maximum(0.0, commit[1:] - commit[:-1]))*fixed_cost
    off_cost = max(0.0, state_0 - commit[0])*fixed_cost
    off_cost = np.sum(np.maximum(0.0, commit[:-1] - commit[1:]))*fixed_cost
    return on_cost + off_cost


def gen_cost(data, result, u):
    """Compute generation cost of a Unitoy solution.
    """
    generation = result.loc[u]["power(p)"].values
    generation_cost = data.loc[u, "C"]
    return sum(generation_cost*generation)


def add_hydro(result, extraResult, toy, v0,hydroC):
    C = hydroC
    F = 0*toy.FCF(0*v0);
    hydro_power = extraResult["hydroPower"]
    hydro_index = pd.MultiIndex.from_tuples([("hydro", i) for i in hydro_power.index])
    hydro_result = pd.DataFrame({"power(p)": hydro_power.values}, index=hydro_index)
    hydro_result["state 0"] = 1.0 #CAS OJO
    hydro_result["commit(u)"] = 1.0 #CAS OJO
    hydro_result["z"] = 0.0 
    hydro_result["y"] = 0.0
    result = result.append(hydro_result)

    hydro_data = pd.DataFrame({"C": C, "F": F, "pmin": np.nan, "pmax": np.nan, "p0": np.nan,
        "ramp": np.nan, "minTimeOn": 0, "minTimeOff": 0}, index=["hydro"])
    hydro_data.index.name = "UT"
    data = toy.data.append(hydro_data)
    return result, data


def ra_price(toy, target_prices, v0, hydroC, a=1.0, b=0.0, beta=0.01, M=1.0e+9):
    """Revenue-adequate prices.
    """
    if toy.primalDict is None:
        print('Primal model not yet built...')
        return None

    # Solve Unitoy model and get results
    optVal, result, extraResult = toy.solveModel()
    if extraResult!={}: result, data = add_hydro(result, extraResult, toy, v0, hydroC)
    units = result.index.get_level_values("unit").unique()
    production_levels = result["power(p)"]    
    if units[-1]=="__C__" and sum(production_levels["__C__"])<1e-4: units=units[:-1]  #OJO CAS

    # Create optimization model for revenue adequate prices
    # Index sets
    time_span = list(range(len(target_prices)))

    # Create model
    m = grb.Model("RAPrice")

    # Create variables
    prices = m.addVars(time_span, name="prices")
    comps = m.addVars(units, time_span, name="compensation")
    unit_comps = m.addVars(units, name="compensation")
    s = m.addVars(units, name="can_compensate", vtype=grb.GRB.BINARY)

    # Add constraints
    m.addConstrs(
        (unit_comps[u] == comps.sum(u, "*") for u in units), 
        name="define_unit_comps"
    )
    m.addConstrs(
        (unit_comps[u] <= fixed_cost(data, result, u)*s[u] for u in units), 
        name="allow_compensate"
    )
    revenue = {u: grb.quicksum(production_levels[u, t + 1]*prices[t] for t in time_span) + 
                  unit_comps[u] for u in units
              }
    m.addConstrs(
        (revenue[u] >= fixed_cost(data, result, u) + gen_cost(data, result, u) for u in units), 
        name="revenue_adequate"
    )
    m.addConstrs(
        (
            revenue[u] <= fixed_cost(data, result, u) + 
            gen_cost(data, result, u) + (1 - s[u])*M for u in units
        ),
        name="no_extra_profit"
    )
    total_cost = sum([fixed_cost(data, result, u) + gen_cost(data, result, u) for u in units])
    m.addConstr(
        grb.quicksum(unit_comps[u] for u in units) <= 
        beta*(grb.quicksum(prices[t]*production_levels[u, t + 1] for u in units for t in time_span) - total_cost),
        name="limit_compensations"
    )

    # Add objetive and solve
    quadnorm = grb.quicksum((
        prices[t] - target_prices[t])*(prices[t] - target_prices[t]) for t in time_span
    )
    abs_prices = m.addVars(time_span, name="abs_prices")
    m.addConstrs((abs_prices[t] >= prices[t] for t in time_span), name="define_abs_pos")
    m.addConstrs((abs_prices[t] >= -prices[t] for t in time_span), name="define_abs_neg")
    m.setObjective(0.5*a*quadnorm + b*grb.quicksum(abs_prices), grb.GRB.MINIMIZE)

    m.setParam( 'OutputFlag', False ) #CAS
    m.optimize()
    #lpName=input('lp name?'); m.write(lpName); #breakpoint() 
    # Create solution and return
    index = [t + 1 for t in time_span]
    mindex = pd.MultiIndex.from_tuples([(u, t + 1) for u in units for t in time_span])
    values = [comps[u, t - 1].x for u, t in mindex]
    result = {
        "demand": pd.Series([prices[t].x for t in time_span], index=index),
        "commit_fixed": pd.Series(values, index=mindex)
    }
        
    return result


def hoe_price(toy, target_prices, v0,hydroC, cup=1.0, cdown=1.0):
    """New O'Neill prices.
    """
    if toy.primalDict is None:
        print('Primal model not yet built...')
        return None

    # Solve Unitoy model and get results
    optVal, result, extraResult = toy.solveModel()
    if extraResult!={}: result, data = add_hydro(result, extraResult, toy, v0, hydroC)
    result, data = add_hydro(result, extraResult, toy, v0,hydroC)
    units = result.index.get_level_values("unit").unique()
    demands = toy.__demand__.copy()
    production_levels = result["power(p)"]    
    if units[-1]=="__C__" and sum(production_levels["__C__"])<1e-4: units=units[:-1]  #OJO CAS

    # Create optimization model for revenue adequate prices
    # Index sets
    time_span = list(range(len(demands)))

    # Create model
    m = grb.Model("New Oneill")

    # Create variables
    prices = m.addVars(time_span, name="prices")
    comps_pos = m.addVars(units, time_span, name="positive_compensation")
    comps_neg = m.addVars(units, time_span, name="negative_compensation")
    dcomps_pos = m.addVars(time_span, name="demand_positive_compensation")
    dcomps_neg = m.addVars(time_span, name="demand_negative_compensation")
    pos_dev = m.addVars(time_span, name="pos_deviantion")
    neg_dev = m.addVars(time_span, name="neg_deviation")
    # In Gurobi the default is positive variables, hence I don't need to add
    # the non-confiscation constraints explictily. 
    profit = m.addVars(units, name="revenue")
    value = m.addVar(name="value")

    # Add constraints
    # TODO: Check if it is plus or minus
    m.addConstr(
        grb.quicksum(demands[t + 1]*(dcomps_pos[t] - dcomps_neg[t]) for t in time_span)
        + grb.quicksum(production_levels[u, t + 1]*(comps_pos[u, t] - comps_neg[u, t])
            for u in units for t in time_span) == 0,
        name="uplift_revenue_neutrality"
    )
    m.addConstrs(
        (
            profit[u] == grb.quicksum(
                production_levels[u, t + 1]*(prices[t] + comps_pos[u, t] - comps_neg[u, t])
                 for t in time_span
            ) - gen_cost(data, result, u) - fixed_cost(data, result, u)
            for u in units
        ),
        name="profit_definition"
    )
    # TODO: Copyting the code from Unitoy, not sure how to access this value
    deficit_cost = 10 * len(time_span) * (data['C'] + data['F']).max() 
    m.addConstr(
        value == grb.quicksum(
            demands[t + 1]*(deficit_cost - prices[t] + dcomps_pos[t] - dcomps_neg[t])
            for t in time_span
        ),
        name="value_definition"
    )
    m.addConstrs(
        (
            (prices[t] - target_prices[t])/target_prices[t] - pos_dev[t] + neg_dev[t] == 0
            for t in time_span
        ),
        name="price_conditioning"
    )
    # Add objetive and solve
    m.setObjective(
        grb.quicksum(demands[t + 1]*dcomps_pos[t] for t in time_span)
        + grb.quicksum(production_levels[u, t + 1]*comps_pos[u, t] 
            for u in units for t in time_span)
        #+ cup*grb.quicksum(pos_dev[t]*pos_dev[t] for t in time_span) 
        #+ cdown*grb.quicksum(neg_dev[t]*neg_dev[t] for t in time_span), 
        + cup*grb.quicksum(pos_dev) + cdown*grb.quicksum(neg_dev), 
        grb.GRB.MINIMIZE
    )

    m.setParam( 'OutputFlag', False ) #CAS
    m.setParam( 'Method', 2 ) #CAS
    m.optimize()
    
    # Create solution and return
    index = [t + 1 for t in time_span]
    mindex = pd.MultiIndex.from_tuples([(u, t + 1) for u in units for t in time_span])
    values = [comps_pos[u, t - 1].x*production_levels[u, t] for u, t in mindex]
    result = {
        "demand": pd.Series([prices[t].x for t in time_span], index=index),
        "commit_fixed": pd.Series(values, index=mindex)
    }

    return result


def summary(toy, price, ticket, v0, hydroC):
    """Create a summary with price informations.
    """
    #print(price.columns)
    columns_names = ["Gen. Cost", "Fixed cost", "Total Cost"]
    for pname in price.columns:
        columns_names += [f"{pname} Gen", f"{pname} Fix", f"{pname} Total"]

    price_summary = pd.DataFrame(columns=columns_names)

    optVal, result, extraResult = toy.solveModel()
    if extraResult!={}: result, data = add_hydro(result, extraResult, toy, v0, hydroC)
    if extraResult=={}: data = toy.data
    units = result.index.get_level_values("unit").unique()
    if units[-1]=="__C__": units=units[:-1]  #OJO CAS


    for u in units:
        res = []
        gen, fixed = gen_cost(data, result, u), fixed_cost(data, result, u)
        res += [gen, fixed, gen + fixed]
        for pname in price.columns:
            gen = (result.loc[u, :]["power(p)"]*price[pname]).sum()
            if (pname == "IP" or pname == "dual") and u == "hydro":
                fixed = 0.0
            else:
                fixed = max(0.0, ticket[pname][u])
            res += [gen, fixed, gen + fixed]
        price_summary.loc[u] = res

    return price_summary


def analyze_prices(prices_summary):
    """Analyze the prices to see if they have good properties.
    """
    error_allowed =  0.9999
    total_cost = prices_summary["Total Cost"]
    total_cols = [i for i in prices_summary.columns if i.endswith("Total")]
    prices_total = prices_summary[total_cols]
    gen_cost = prices_summary["Gen. Cost"]
    gen_cols = [i for i in prices_summary.columns if i.endswith("Gen")]
    prices_gen = prices_summary[gen_cols]
    gen_cols_short = [i[:-4] for i in gen_cols]
    
    pt=prices_total.T
    at=[pt[col].values-total_cost[col] for col in pt.columns]
    profits=pd.DataFrame(np.round(at),index=prices_total.index,columns=[c+' profit' for c in gen_cols_short])
    margins = prices_gen.T-prices_summary['Total Cost']
    margins.index=[c+' Margin' for c in gen_cols_short]
    totalUp=prices_summary[[c+' Fix' for c in gen_cols_short]].sum(axis=0)
    return margins.T, profits,totalUp
