###
import numpy as np
import pandas as pd
import gurobipy as grb
###


def model1(basicData, demand=None, param=None, unitList=None, FCFTable=None):
    '''
    param: [None, dict] (default None). If None, it is added a extra (very expensive) unit 'C'.
     Also can be passed a dictionry: 
    {'C': float (unit cost), 'pmax': float (unit max capacity}
    if the dictionary does not have some of the keys, no unit will be added
    Any other value (bool, number, etc) will have the same effect
    '''

    data = basicData.data
    if unitList is None:
        unitList = list(data.index)
    else:
        unitList = list(unitList)
        data = data.loc[unitList]
    D = demand

    time = list(D.index)

    aux = data.loc[unitList]
    defaultDeficit = {'C': 10 * len(time) * (aux['C'] + aux['F']).max() , 'pmax': 10 * D.max()}

    if  param is None: 
        deficit = defaultDeficit

    elif isinstance(param , dict):
        if 'deficit' in param:
            deficit = param['deficit'] 
            if 'C' not in param['deficit']: 
                print('Buiding model warning: deficit extra unit dictionary does not have "C"  key.  Default will be used') 
                deficit['C'] = defaultDeficit['C']
            if 'pmax' not in param['deficit']: 
                print('Buiding model warning: deficit extra unit dictionary does not have "pmax"  key.  Default will be used') 
                deficit['pmax'] = defaultDeficit['pmax']
        else:
            deficit=None
    else:
        deficit = None

    basicData.__deficit__=deficit

    model = grb.Model('unitoy-case0')
    model.setParam('LogtoConsole', 0)

    if deficit is not None:
        p = model.addVars(unitList + ['__C__'], time,  vtype=grb.GRB.CONTINUOUS, lb=-grb.GRB.INFINITY,  name='p')
        model.addConstrs((p['__C__', t] >= 0 for t in  time), name='__C__pmin')
    else:
        p = model.addVars(unitList , time,  vtype=grb.GRB.CONTINUOUS, lb=-grb.GRB.INFINITY,  name='p')

    #maping p possitve if pmin is so
    model.addConstrs(( p[unit, t ]>=0 for unit in unitList for t in time if  data.loc[unit, 'pmin']>=0),  name='lbPppos') 
    

    u = model.addVars(unitList, time,  vtype=grb.GRB.BINARY, name='u') # unit online
    y = model.addVars(unitList, time,  vtype=grb.GRB.BINARY, name='y') #unit is started-up
    z = model.addVars(unitList, time,  vtype=grb.GRB.BINARY, name='z') #unit is shut-down A.J.Conejo J.M.Arroyo, Modeling of start-up and shut-down power trajectories of thermal units, IEEE Trans. Power Syst. v19, (n.3) (2004) 1562–1568.
    modelVar = {'power(p)': p, 'commit(u)': u, 'z': z, 'y':y}
    extraVar = {}
    extraCon = {}
    extraConLinExpr = {}

    model.update()

    # start-up, shutdown on line constraitns
    model.addConstrs((y[unit,time[i]] >= ((u[unit,time[i]] - u[unit, time[i-1]]) if i >0 else (u[unit,time[i]] - data.loc[unit, 'state0'])) 
        for unit in unitList for i in range(len(time))), name='startUp') # starting up

    model.addConstrs((z[unit,time[i]] >= ((u[unit,time[i-1]] - u[unit, time[i]]) if i >0 else (data.loc[unit, 'state0'] - u[unit,time[i]])) 
        for unit in unitList for i in range(len(time))), name='shutDown') # shutdown 

    # Constraints
    if len(time) == 1: # static case: there is no need for complex constraints like ramps, and minminum time on/off 
        model.addConstrs((data.loc[unit,'pmin'] * u[unit, time[i]] <= p[unit, time[i]] for unit in data.index 
            for i in range(len(time))),  name='pmin') 
        model.addConstrs((data.loc[unit,'pmax'] * u[unit, time[i]] >= p[unit, time[i]] for unit in data.index 
            for i in range(len(time))),  name='pmin') 

    else: # for dynamic demand it is required a more complex system of constraints
        #constraints for zero pmin units
        zeroPminBoolIndices = np.abs(data['pmin'])<1e-4
        auxInd = data.index[zeroPminBoolIndices]

        if len(auxInd) > 0: 
            # capacity constraints 
            model.addConstrs((p[unit, time[i]]>= data.loc[unit, 'pmin'] for unit in auxInd for i in range(len(time))),  name='zeroPmin')
            model.addConstrs(( p[unit, time[i]]<= data.loc[unit,'pmax'] *u[unit, time[i]] 
                for unit in auxInd for i in range(len(time))),  name='zeroPmin_pmaxCon')

            # ramp constraints
            if 'ramp' in data.columns:
                rampAuxInd = data.index[ zeroPminBoolIndices & (~data['ramp'].isna())]
                if len(rampAuxInd) >0: 
                    model.addConstrs((p[unit, time[i]] - (p[unit, time[i - 1]] if i>0 else data.loc[unit, 'p0']) <=  data.loc[unit, 'ramp'] 
                        for unit in rampAuxInd for i in range(len(time))), name='zeroPmin_rampUp')

                    model.addConstrs(((p[unit, time[i - 1]] if i>0 else data.loc[unit, 'p0']) - p[unit, time[i]]  <=  data.loc[unit, 'ramp'] 
                        for unit in rampAuxInd for i in range(len(time))), name='zeroPmin_rampDown')

        #constraints for positive pmin units
        auxInd = data.index[(~zeroPminBoolIndices) & (data['pmin']>0)]
        if len(auxInd) > 0: # positive pmin units  capacity constraints. start-up and shoutdwon  durataions are 1. 
            model.addConstrs(( p[unit, time[i]]>=0 for unit in auxInd for i in range(len(time))),  name='lbPppos') 
            model.addConstrs((data.loc[unit,'pmin'] * (u[unit, time[i]] -y[unit, time[i]] - (z[unit, time[i+1]] if i+1 < len(time) else 0))<= p[unit, time[i]] for unit in auxInd for i in range(len(time))),  name='pmin') 
            if len(time)> 1: # only considered for static demand
                model.addConstrs((data.loc[unit, 'pmax'] * (u[unit, time[i]] -y[unit, time[i]]) + data.loc[unit, 'pmin']* y[unit, time[i]] >= p[unit, time[i]] for unit in auxInd for i in range(len(time))),  name='pmax_startUP') 
                model.addConstrs((data.loc[unit, 'pmax'] * (u[unit, time[i]] -(z[unit, time[i+1]] if i+1 < len(time) else 0)) + data.loc[unit, 'pmin']*(z[unit, time[i+1]] if i+1 < len(time) else 0) >= p[unit, time[i]] 
                for unit in auxInd for i in range(len(time))),  name='pmax_shutDown')
            else:
                model.addConstrs((data.loc[unit, 'pmax'] * u[unit, time[i]] >= p[unit, time[i]] for unit in auxInd for i in range(len(time))),  name='pmax_4_positive_pmin_onlyOneT') 

        

            # ramping
            if 'ramp' in data.columns:
                rampAuxInd = data.index[ (~zeroPminBoolIndices) & (~data['ramp'].isna())] 
                model.addConstrs((p[unit, time[i]] - (p[unit, time[i - 1]] if i>0 else data.loc[unit, 'p0']) <= data.loc[unit, 'pmax'] * y[unit, time[i]] + data.loc[unit, 'ramp'] * (u[unit, time[i]] -y[unit, time[i]]) 
                    for unit in rampAuxInd for i in range(len(time))), name='ramp_up') 

                model.addConstrs(((p[unit,time[i-1]] if i>0 else data.loc[unit, 'p0'])- p[unit,time[i]] <= data.loc[unit, 'pmax'] * z[unit, time[i]] + data.loc[unit, 'ramp'] *((u[unit, time[i-1]] if i>0 else data.loc[unit, 'state0']) -z[unit, time[i]]) 
                    for unit in rampAuxInd  for i in range(len(time))), name='ramp_down')

        #constraints for negative pmin units
        auxInd = data.index[(~zeroPminBoolIndices) & (data['pmin']<0)]
        if len(auxInd) > 0: # positive pmin units  capacity constraints. start-up and shoutdwon  durataions are 1.  
            model.addConstrs(( p[unit, time[i]]>= data.loc[unit,'pmin'] *u[unit, time[i]] 
                for unit in auxInd for i in range(len(time))),  name='NegPmin_pminCon')
            model.addConstrs(( p[unit, time[i]]<= data.loc[unit,'pmax'] *u[unit, time[i]] 
                for unit in auxInd for i in range(len(time))),  name='NegPmin_pmaxCon')


        # minimum Time On (if a unit state0=1, then we ask it to be on at minTimeOn periods from the first one)
        if 'minTimeOn'in data.columns: #it is used min([horizon, minTimeOn]) in order to avoid unfeasibility
            tabaux = data.loc[~data['minTimeOn'].isna()]
            model.addConstrs((grb.quicksum(u[unit,time[i+j]] for j in range(int(tabaux.loc[unit,'minTimeOn'])) if i+j<len(time)) 
                >= min([len(time), tabaux.loc[unit, 'minTimeOn']]) *( (u[unit,time[i]] -  u[unit,time[i-1]]) if i>0 else ((u[unit,time[i]] if tabaux.loc[unit,'state0']<0.5 else 1)))
                for unit in tabaux.index  for i in range(len(time))), name='minTimeOn')

        # minimum Time Off(if a unit state0=0 we do not include the constraint)
        if 'minTimeOff'in data.columns:#it is used min([horizon, minTimeOn]) in order to avoid unfeasibility
            tabaux = data.loc[~data['minTimeOff'].isna()]
            model.addConstrs((grb.quicksum(1-u[unit,time[i+j]] for j in range(int(tabaux.loc[unit,'minTimeOff'])) if i+j<len(time)) 
                >= min([len(time), tabaux.loc[unit, 'minTimeOff']]) *((u[unit,time[i-1]] -  u[unit, time[i]]) if i>0 else (tabaux.loc[unit,'state0']-u[unit,time[i]]))
                for unit in tabaux.index  for i in range(len(time))), name='minTimeOff')

    # deficit cap
    if deficit is not None:
        model.addConstrs((p['__C__', t] <= deficit['pmax'] for t in time), name='deficit_cap')

    
    ## Modeling Hydro part if required
    if param is not None:
        if 'hydroSystem' in param:
            hydroSystem = param['hydroSystem']
            if 'v0' in hydroSystem:
                v0 = hydroSystem['v0']
            if 'eta' in hydroSystem:
                eta = hydroSystem['eta']
            if 'hydroData' in hydroSystem:
                hData = hydroSystem['hydroData']

            hp = model.addVars( time,  vtype=grb.GRB.CONTINUOUS, lb=0.0,  name='hp')
            hvol = model.addVars( time,  vtype=grb.GRB.CONTINUOUS, lb=0.0,  name='hvol')
            extraVar.update({'hydroVol': hvol, 'hydroPower': hp})


            # min max volume constraints
            model.addConstrs((hvol[time[i]] >= hData.loc[time[i], 'vmin']  for i in range(len(time))),  name='vmin')
            model.addConstrs((hvol[time[i]] <= hData.loc[time[i], 'vmax']  for i in range(len(time))),  name='vmax')


            # hydro balance
            model.addConstrs((hvol[ time[i]] == hData.loc[time[i], 'I'] + (hvol[time[i - 1]] if i>0 else v0) - eta * hp[time[i]]
                for i in range(len(time))),  name='hydroBalance')

            # FCF 
            if FCFTable is None:
                print('Warning: No FCF provided!!! It will be cosidered as being zero')
                FCF = 0
            else: 
                FCF = model.addVar(vtype=grb.GRB.CONTINUOUS, lb=0.0,  name='FCF')
                auxTab = FCFTable[[cc for cc in FCFTable.columns if 'hydro' in cc.lower()]]
                FCFCon=model.addConstrs((FCF >= FCFTable.loc[i, 'constant'] + auxTab.loc[i].mean() * hvol[time[-1]] 
                    for i in FCFTable.index), name='FCFCon')
                extraCon.update({'FCFCon': FCFCon})


        else:
            hp = {t: 0 for t in time} # null hydropower
            FCF = 0 #future cost function
    else:
        hp = {t: 0 for t in time} # null hydropower
        FCF = 0 #future cost function

        


    # demand
    demandConLinExpr = {t:p.sum('*',t) +hp[t] - D[t]  for t in time}
    demandCon= model.addConstrs((demandConLinExpr[t] == 0 for t in time), name='demand')
    # demandCon= model.addConstrs((p.sum('*',t) +hp[t] - D[t] == 0 for t in time), name='demand')
    extraCon.update({'demand': demandCon})
    extraConLinExpr.update({'demand': demandConLinExpr}) 

    model.update()
    # model.write('jj.lp')

    # Setting the objective function
    objF = grb.quicksum( data['C'][unit] * p.sum(unit,'*') +
            data['F'][unit] * (z.sum(unit, '*') +y.sum(unit,'*'))  for unit in unitList)  + 0* FCF
    if deficit is not None:
        objF = objF + deficit['C'] * p.sum('__C__','*')

    model.setObjective(objF, sense=grb.GRB.MINIMIZE)
    
    return dict(time=time, unitList=unitList, model=model, var=modelVar, extraCon=extraCon, extraVar=extraVar, extraConLinExpr=extraConLinExpr)
